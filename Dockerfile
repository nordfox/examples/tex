FROM ubuntu:20.04
ENV DEBIAN_FRONTEND="noninteractive"
RUN apt-get update && apt-get install -y \
    wget \
    locales \
    texlive \
    texlive-lang-german \
    texlive-latex-extra \
    texlive-bibtex-extra \
    biber \
    texlive-science \
 && rm -rf /var/lib/apt/lists/*

RUN wget -q https://www.tug.org/fonts/getnonfreefonts/install-getnonfreefonts \
 && texlua ./install-getnonfreefonts \
 && getnonfreefonts --sys -a
 
RUN locale-gen de_DE.UTF-8 && update-locale LANG=de_DE.UTF-8
ENV LANG=de_DE.UTF-8
